const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bcrypt = require('bcrypt');
const { PORT = 3000 } = process.env;
const { Sequelize, QueryTypes } = require('sequelize');
require('dotenv').config();

// Initialize the App.
const app = express();

// Setup Middleware applications.
app.use(express.json());
app.use(cookieParser());
// Expose JavaScript, Images and CSS files under the alias public/
app.use('/public', express.static(path.join(__dirname, 'www')));

// initialize the database connection
const sequelize = new Sequelize({
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_DATABASE,
    dialect: process.env.DB_DIALECT
});

// Check if we can establish a connection to the database.
sequelize.authenticate().then(d => {

    // Successfully connected to the Database.
    app.use(async (req, res, next) => { //  NOTE: This is an async function -> You can use await.
        let dbUserId = 0;

        // 1. Allow / to pass through 
        // If user tries to load /dashboard it should redirect to / -- See slides.

        // Dewalds version
        // const publicPaths = ['/', '/api/register', '/api/login'];
        // if ( publicPaths.includes( req.paths)) {
        //    return next();
        // }
        console.log('middle 0');

        // Path through to be able to log in and create a session
        if (req.path === '/' || req.path === '/api/register' || req.path === '/api/login') {
            return next();
        }

        console.log('middle 1');

        // 2. Check if the req cookie -> token exists.
        if (!req.cookies.token) {
            return res.redirect('/');
        }

        console.log('middle 2');

        // 3. get the token from the request cookie 
        const { token } = req.cookies;

        console.log('middle 3');

        // 4. Check the database for the token and if it is active = 1
        try {
            // const [ session ] = await sequelize.query(`SELECT userId, active FROM userSession WHERE token='${token}'`, { // No good - SQL injection possible here
            // Use provided methods in seequlize instead as below.
            const [session] = await sequelize.query(`SELECT userId, active FROM userSession WHERE token= :token`, {
                type: QueryTypes.SELECT,
                replacements: {
                    token: token
                }
            });

            dbUserId = session.userId;
            // Skicka message om det inte finns en aktiv cookie
            if (session.active != 1) {
                return res.status(403).json({ message: 'Session cookie not valid' });
            }

        } catch (e) {
            console.error(e);
        }

        console.log('middle 4');

        // 5. Check if the user linked to the token exists
        try {
            const [user] = await sequelize.query(`SELECT username FROM user WHERE userId=${dbUserId}`, {
                type: QueryTypes.SELECT
            });

            // 6. Return a status 401 If it is expired or the user does not Exists
            if (user.username == undefined) {
                return res.status(401).json({ message: 'User does not exist' });
            }

        } catch (e) {
            console.error(e);
        }
        console.log('middle last');
        return next();
    });

    app.get('/', (req, res) => { // this is our index page (or login)
        return res.status(200).sendFile(path.join(__dirname, 'www', 'index.html'));
    });

    app.get('/dashboard', (req, res) => { // Must be a protected page. Can't access without token cookie.
        return res.status(200).sendFile(path.join(__dirname, 'www', 'dashboard.html'));
    });

    app.post('/api/login', async (req, res) => { // Authentication Request.
        // Check the login
        // 1. Get the username and password from the req.body
        console.log('start of login api');

        const { username, password} = req.body;
        let dbUserId = 0;

        if (!username || !password) {
            return res.status(400).json({
                message: 'Please provide a username and password'
            });
        }

        // 2. Check if the username exists (Select the user based on the username)
        try {
            await sequelize.authenticate();
            console.log('Database connection established...');
            const user = await sequelize.query(`SELECT userId, password FROM user WHERE username= :username`, {
                type: QueryTypes.SELECT,
                replacements: {
                    username: username
                }
            });

            if (user && user.length == 0) { // No matching user
                return res.status(401).json({ message: 'No such user exist' });
            }

            dbUserId = user[0].userId;

            // 3. Check if the password in the db matches the password given.
            // Load hash from your password DB. Sample using Async/Await:
            const result = await bcrypt.compare(password, user[0].password);
            if (result == false) {
                return res.status(401).json({ message: 'User does not exist' });
            }

            // 4. If true Generate a token using the HASH_SECRET from the .env file (it will be on the process.env)
            const token = await bcrypt.hash(process.env.HASH_SECRET, 10);

            // 4.2 Delete token in databse if any (Remove if it should be possible to jump between devices)
            await sequelize.query(`DELETE FROM userSession WHERE userId='${dbUserId}'`, {
                type: QueryTypes.DELETE
            });

            // 4.5 Store the token in the database
            await sequelize.query(`INSERT INTO userSession ( userId, token ) VALUES ( '${dbUserId}', '${token}')`, {
                type: QueryTypes.INSERT
            });
            // 5. NB - Send the token back to the client.
            return res.status(201).json({
                status: 201,
                token: token
            });

        } catch (e) {
            return res.status(500).json({
                message: e.toString()
            });
            console.error(e);
        }

    })

    app.post('/api/register', async (req, res) => {
        // const username = 'Isseorg';
        // const password = 'topSecretIsse';
        const username = 'Isse';
        const password = 'topSecretIsse';
        try {
            const hashPassword = await bcrypt.hash(password, 10);
            const result = await sequelize.query('INSERT INTO user (username, password) VALUE (:username, :password)', {
                type: QueryTypes.INSERT,
                replacements: {
                    username: username,
                    password: hashPassword
                }
            })
        } catch (e) {
            return res.json(e);
        }
        return res.json('Done...');
    });

    app.listen(PORT, () => console.log(`Server has started on port ${PORT}...`));

}).catch(e => {
    console.error(e);
})



