class CookieManager {
    // Method
    static setCookie(name, data) {
        document.cookie = `${name}=${data}; max-age=900`;
    }
    static getCookie(name) {
        let cookieArr = document.cookie.split('; ');
        let nameValuePair = [];
        //let returnValue = false;
        for (let i = 0; i < cookieArr.length; i++) {
            nameValuePair = cookieArr[i].split('=');
            if (name === nameValuePair[0]) {
                return nameValuePair[1];
            }
        }
        return false;
    }
    static clearCookie(name) {
        document.cookie = `${name}=; max-age=-900`;
    }
}

App.prototype.init = function () {
    this.elBtnLogin.addEventListener('click', async e => {
        e.preventDefault();

        try {
            console.log('hello');
            alert('hello');
            let data = { elUsername.value, elPassword.value };
            const result = await fetch('http://localhost:3000/api/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data),
            }).then(r => r.json());
            alert(result);
            // 1. Store the token in a cookie called token.
            CookieManager.setCookie("token", result);

            // 2. Redirect to Dashboard.
            await fetch('http://localhost:3000/dashboard', {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + CookieManager.getCookie("token")
                },
            }).then(r => r.json());

        } catch (e) {
            alert(e.toString());
        }


    });
}

new App().init();