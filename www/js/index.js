class CookieManager {
    // Method
    static setCookie(name, data) {
        document.cookie = `${name}=${data}; max-age=900`;
    }
    static getCookie(name) {
        let cookieArr = document.cookie.split('; ');
        let nameValuePair = [];
        //let returnValue = false;
        for (let i = 0; i < cookieArr.length; i++) {
            nameValuePair = cookieArr[i].split('=');
            if (name === nameValuePair[0]) {
                return nameValuePair[1];
            }
        }
        return false;
    }
    static clearCookie(name) {
        document.cookie = `${name}=; max-age=-900`;
    }
}

function App() {
    this.elUsername = document.getElementById('username');
    this.elPassword = document.getElementById('password');
    this.elBtnLogin = document.getElementById('btn-login');
}

App.prototype.init = function () {
    this.elBtnLogin.addEventListener('click', async e => {
        // Prevent the form from Submitting.
        e.preventDefault();

        if (!this.elUsername.value || !this.elPassword.value) {
            return alert('Please enter a username and password');
        }

        try {
            console.log('before login attempt');
            console.log(this.elUsername.value.trim());
            console.log(this.elPassword.value.trim());
            const result = await fetch('http://localhost:3000/api/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    username: this.elUsername.value.trim(),
                    password: this.elPassword.value.trim()
                })
            }).then(r => r.json());

            console.log('result');
            console.log(result);

            if (result.status >= 400) {
                alert(result.message);
                return;
            }

            console.log('result from server login api:');
            console.log(result.token);
            // 1. Store the token in a cookie called token.
            CookieManager.setCookie("token", result.token);

            console.log('About to redirect');

            // 2. Redirect to Dashboard.
            window.location.href = '/dashboard';


        } catch (e) {
            alert(e.toString());
        }


    });
}

new App().init();
